import pyglet
from pyglet.shapes import Circle
from random import randrange

from dataclasses import dataclass

window = pyglet.window.Window(resizable=True)

@dataclass
class Ball:
    circle: Circle
    x_velocity: int
    y_velocity: int

    def on_edge(self, window):
        if self.circle.x + self.circle.radius >= window.width:
            self.x_velocity = -1 * self.x_velocity
        if self.circle.y + self.circle.radius >= window.height:
            self.y_velocity = -1 * self.y_velocity
        if self.circle.x - self.circle.radius <= 0:
            self.x_velocity = -1 * self.x_velocity
        if self.circle.y - self.circle.radius <= 0:
            self.y_velocity = -1 * self.y_velocity

    def update(self):
        self.circle.x += self.x_velocity
        self.circle.y += self.y_velocity
        self.on_edge(window)

    def draw(self):
        self.circle.draw()

batch = pyglet.graphics.Batch()

balls = []
for i in range(500):
    radius = 10
    start_x = randrange(radius, window.width-radius)
    start_y = randrange(radius, window.height-radius)
    circle = Circle(start_x, start_y, radius, color=(round(i*(256/50)), round((50-i)*(225/50)), 90), batch=batch)
    vel_x = randrange(-6, 6)
    vel_y = randrange(-6, 6)
    ball = Ball(circle, vel_x, vel_y)
    balls.append(ball)

@window.event
def on_draw():
    window.clear()
    # for ball in balls:
    #     ball.draw()
    batch.draw()

def update(dt):
    for ball in balls:
        ball.update()

pyglet.clock.schedule_interval(update, 1/60)

pyglet.app.run()
